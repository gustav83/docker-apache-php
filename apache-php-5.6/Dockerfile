
FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y --no-install-recommends software-properties-common
RUN LC_ALL=C.UTF-8 apt-add-repository -y ppa:ondrej/php
RUN LC_ALL=C.UTF-8 apt-add-repository -y ppa:ondrej/apache2
RUN apt-key update
RUN apt-get update
RUN apt-get install -y \
    php5.6 \
    php5.6-apcu \
    php5.6-cli \
    php5.6-common \
    php5.6-curl \
    php5.6-fpm \
    php5.6-gd \
    php5.6-gmp \
    php5.6-imagick \
    php5.6-intl \
    php5.6-json \
    php5.6-mbstring \
    php5.6-mcrypt \
    php5.6-mongo \
    php5.6-mysql \
    php5.6-readline \
    php5.6-redis \
    php5.6-soap \
    php5.6-sqlite \
    php5.6-tidy \
    php5.6-xdebug \
    php5.6-xml \
    php5.6-xmlrpc \
    php5.6-zip \
    apache2 \
    libapache2-mod-php5.6 \
    unzip \
    libimage-exiftool-perl \
    imagemagick \
    git \
    jpegoptim \
    optipng \
    pngquant \
    gifsicle \
    curl

RUN ln -sf /dev/stderr /var/log/apache2/error.log
RUN ln -sf /dev/stdout /var/log/apache2/access.log
RUN ln -sf /dev/stdout /var/log/apache2/other_vhosts_access.log
RUN a2dismod mpm_event && a2enmod mpm_prefork
RUN a2enmod rewrite
RUN a2enmod ssl
RUN a2enmod http2
RUN a2enmod expires
RUN a2enmod headers
RUN rm /etc/apache2/sites-enabled/*
RUN mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/050-default.conf
RUN mv /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/050-default-ssl.conf
RUN a2ensite 050-default
RUN a2ensite 050-default-ssl
RUN update-alternatives --set php /usr/bin/php5.6
RUN apache2ctl configtest
RUN phpdismod -s cli xdebug
ADD composer-setup.sh /
RUN chmod +x composer-setup.sh
RUN /composer-setup.sh

EXPOSE 80
EXPOSE 443

ADD start.sh /
RUN chmod +x start.sh

CMD ["/start.sh"]

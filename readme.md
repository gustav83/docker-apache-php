Apache docker images for different versions of PHP.

**NOT FOR PRODUCTION USE**

# Tags
- latest
- 5.3 - Ubuntu 12.04 - EOL!
- 5.5 - Ubuntu 14.04
- 5.6 - Ubuntu 16.04 + ondrej PPA with http2
- 7.0 - Ubuntu 16.04 + ondrej PPA with http2
- 7.1 - Ubuntu 16.04 + ondrej PPA with http2
- 7.2 - Ubuntu 16.04 + ondrej PPA with http2
- 7.3 - Ubuntu 16.04 + ondrej PPA with http2

# Usage
```
docker run --rm -v "$(pwd):/var/www/html" -w /app gustav83/apache-php:7.0
docker run --rm -v "$(pwd):/var/www/html" -v"my-apache-config:/etc/apache2/apache2.conf:ro" -w /app gustav83/apache-php:7.0
```

# Notes
HTTP/2 does not work with prefork, see https://http2.pro/doc/Apache. 

# TODO
- Rename default site to 000-default?
- Remove default SSL site?
